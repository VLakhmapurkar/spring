package edu.vishal.spring.learn.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import edu.vishal.spring.learn.springboot.configuration.JpaConfiguration;



@Import(JpaConfiguration.class)
@SpringBootApplication(scanBasePackages={"edu.vishal.spring.learn.springboot"})// same as @Configuration @EnableAutoConfiguration @ComponentScan
public class SpringBootAngularMain {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAngularMain.class, args);
	}
}
